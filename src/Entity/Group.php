<?php

namespace App\Entity;

use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\Table(name: '`group`')]
class Group
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 60)]
    private $name;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $lastArchivedAt;

    #[ORM\Column(type: 'string', length: 255)]
    private $linkToken;

    #[ORM\ManyToOne(targetEntity: Event::class, inversedBy: 'Groups')]
    #[ORM\JoinColumn(nullable: false)]
    private $Event;

    #[ORM\OneToMany(mappedBy: 'idGroup', targetEntity: Ticket::class)]
    private $tickets;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastArchivedAt(): ?\DateTimeInterface
    {
        return $this->lastArchivedAt;
    }

    public function setLastArchivedAt(?\DateTimeInterface $lastArchivedAt): self
    {
        $this->lastArchivedAt = $lastArchivedAt;

        return $this;
    }

    public function getLinkToken(): ?string
    {
        return $this->linkToken;
    }

    public function setLinkToken(string $linkToken): self
    {
        $this->linkToken = $linkToken;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->Event;
    }

    public function setEvent(?Event $Event): self
    {
        $this->Event = $Event;

        return $this;
    }

    public function generateLinkToken(): string
    {
        $linkToken = random_bytes(NB_CHAR_ADMIN_TOKEN);

        return sha1($linkToken);
    }

    /**
     * @return Collection<int, Ticket>
     */
    public function getTickets(): Collection
    {
        return $this->tickets;
    }

    public function addTicket(Ticket $ticket): self
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets[] = $ticket;
            $ticket->setIdGroup($this);
        }

        return $this;
    }

    public function removeTicket(Ticket $ticket): self
    {
        if ($this->tickets->removeElement($ticket)) {
            // set the owning side to null (unless already changed)
            if ($ticket->getIdGroup() === $this) {
                $ticket->setIdGroup(null);
            }
        }

        return $this;
    }
}
