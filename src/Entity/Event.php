<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

const NB_CHAR_ADMIN_TOKEN = 50;

#[ORM\Entity(repositoryClass: EventRepository::class)]
class Event
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 60)]
    private $name;

    #[ORM\Column(type: 'string', length: 150)]
    private $email;

    #[ORM\Column(type: 'string', length: 255)]
    private $adminLinkToken;

    #[ORM\OneToMany(mappedBy: 'Event', targetEntity: Group::class)]
    private $Groups;

    public function __construct()
    {
        $this->Groups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAdminLinkToken(): ?string
    {
        return $this->adminLinkToken;
    }

    public function setAdminLinkToken(string $adminLinkToken): self
    {
        $this->adminLinkToken = $adminLinkToken;

        return $this;
    }

    public function generateAdminToken(): string
    {
        $adminToken = random_bytes(NB_CHAR_ADMIN_TOKEN);

        return sha1($adminToken);
    }

    /**
     * @return Collection<int, Group>
     */
    public function getGroups(): Collection
    {
        return $this->Groups;
    }

    

    public function addGroup(Group $group): self
    {
        if (!$this->Groups->contains($group)) {
            $this->Groups[] = $group;
            $group->setEvent($this);
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->Groups->removeElement($group)) {
            // set the owning side to null (unless already changed)
            if ($group->getEvent() === $this) {
                $group->setEvent(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
