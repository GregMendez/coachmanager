<?php

namespace App\Entity;

use App\Repository\TicketRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TicketRepository::class)]
class Ticket
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'text')]
    private $request;

    #[ORM\Column(type: 'datetime')]
    private $createAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $updateAt;

    #[ORM\ManyToOne(targetEntity: Group::class, inversedBy: 'tickets')]
    #[ORM\JoinColumn(nullable: false)]
    private $idGroup;

    #[ORM\ManyToOne(targetEntity: Status::class, inversedBy: 'tickets')]
    #[ORM\JoinColumn(nullable: true)]
    private $Status;

    #[ORM\ManyToOne(targetEntity: Priority::class, inversedBy: 'tickets')]
    #[ORM\JoinColumn(nullable: false)]
    private $Priority;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequest(): ?string
    {
        return $this->request;
    }

    public function setRequest(string $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getIdGroup(): ?Group
    {
        return $this->idGroup;
    }

    public function setIdGroup(?Group $idGroup): self
    {
        $this->idGroup = $idGroup;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->Status;
    }

    public function setStatus(?Status $Status): self
    {
        $this->Status = $Status;

        return $this;
    }

    public function getPriority(): ?Priority
    {
        return $this->Priority;
    }

    public function setPriority(?Priority $Priority): self
    {
        $this->Priority = $Priority;

        return $this;
    }
}
