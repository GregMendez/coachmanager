<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Event;
use App\Form\EventFormType;


class EventController extends AbstractController
{
    

    #[Route('/', name: 'app_event_creation')]
    public function newEvent(Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $repository = $doctrine->getRepository(Event::class);
        $events = $repository->findAll();

        $event = new Event();

        $adminToken = $event->generateAdminToken();

        $form = $this->createForm(EventFormType::class, $event);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $eventName = $form->get('name')->getData();
                $eventEmail = $form->get('email')->getData();

                $event->setName($eventName);
                $event->setEmail($eventEmail);
                $event->setAdminLinkToken($adminToken);

                $entityManager->persist($event);
                $entityManager->flush();

                $eventId = $event->getId();


                return $this->redirectToRoute('event_groups', ['adminToken' => $adminToken, 'eventId' => $eventId, 'message' => ""]);
            }
        }
        

        return $this->renderForm('event/index.html.twig', [
            'form' => $form,
            'events' => $events,
            'adminToken' => $adminToken
        ]);
    }

    
}
