<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\Collections\ArrayCollection;

use App\Form\GroupFormType;
use App\Form\NewTicketFormType;

use App\Entity\Group;
use App\Entity\Event;
use App\Entity\Priority;
use App\Entity\Ticket;

class GroupController extends AbstractController
{
    #[Route('/event/{adminToken}/groups/', name: 'event_groups')]
    public function createGroups(ManagerRegistry $doctrine, Request $request, string $adminToken): Response
    {
        $entityManager = $doctrine->getManager();

        $group = new Group();

        $eventId = $request->query->get('eventId');
        $message = $request->query->get('message');

        $repository = $doctrine->getRepository(Event::class);
        $event = $repository->find($eventId);

        $groups = $event->getGroups();
        

        $linkToken = $group->generateLinkToken();

        $form = $this->createForm(GroupFormType::class, $group);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $groupName = $form->get('name')->getData();
            

            $repository = $doctrine->getRepository(Event::class);
            $event = $repository->find($eventId);

            $group->setName($groupName);
            $group->setLinkToken($linkToken);
            $group->setEvent($event);

            $entityManager->persist($group);
            $entityManager->flush();

            return $this->redirectToRoute('event_groups', ['adminToken' => $adminToken, 'eventId' => $eventId, 'message' => $message]);
            
        }

        return $this->renderForm('group/index.html.twig', [
            'form' => $form,
            'groups' => $groups,
            'adminToken' => $adminToken,
            'linkToken' => $linkToken,
            'eventId' => $eventId,
            'message' => $message
        ]);
    }

    #[Route('/event/{adminToken}/groups/remove/', name: 'event_group_remove')]
    public function removeGroup(ManagerRegistry $doctrine, Request $request, string $adminToken): Response
    {
        $groupId = $request->query->get('groupId');
        $eventId = $request->query->get('eventId');

        $repository = $doctrine->getRepository(Group::class);
        $group = $repository->find($groupId);
        $tickets = $group->getTickets();
        

        if($tickets->count() == 0)
        {
            $repository->remove($group);
            $message = "groupe supprimé";
        } else {
            $message = "impossible de supprimer le groupe";
        }
        
        
        

        return $this->redirectToRoute('event_groups', ['adminToken' => $adminToken, 'eventId' => $eventId, 'message' => $message]);        
    }

    #[Route('/group/{linkToken}/', name: 'group_tickets')]
    public function createTicket(ManagerRegistry $doctrine, Request $request, string $linkToken): Response
    {
        $entityManager = $doctrine->getManager();

        $ticket = new Ticket();

        $groupId = $request->query->get('groupId');
        $repository = $doctrine->getRepository(Group::class);
        $group = $repository->find($groupId);
        $event = $group->getEvent();


        $tickets = $group->getTickets();
        
        $form = $this->createForm(NewTicketFormType::class, $ticket);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $ticketRequest = $form->get('request')->getData();
            $priorityLevel = $form->get('Priority')->getData();

            $repository = $doctrine->getRepository(Priority::class);
            $priority = $repository->findOneByWeight($priorityLevel);
            $createAt = new \DateTime();            
            
            $ticket->setPriority($priority);
            $ticket->setRequest($ticketRequest);
            $ticket->setCreateAt($createAt);
            $ticket->setIdGroup($group);
            
            

            $entityManager->persist($ticket);
            $entityManager->flush();

            return $this->redirectToRoute('group_tickets', ['linkToken' => $linkToken, 'groupId' => $groupId]);
            
        }

        return $this->renderForm('group/showGroup.html.twig', [
            'form' => $form,
            'group' => $group,
            'linkToken' => $linkToken,
            'tickets' => $tickets,
            'event' => $event
        ]);
    }

}
