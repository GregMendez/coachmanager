<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

use App\Form\NewTicketFormType;
use App\Form\EditTicketFormType;

use App\Entity\Ticket;
use App\Entity\Priority;
use App\Entity\Group;
use App\Entity\Event;

class TicketController extends AbstractController
{
    #[Route('/event/{adminToken}/tickets/', name: 'event_tickets')]
    public function showTickets(ManagerRegistry $doctrine, Request $request, string $adminToken): Response
    {
        $eventId = $request->query->get('eventId');

        $repository = $doctrine->getRepository(Event::class);
        $event = $repository->find($eventId);

        $groups = $event->getGroups();


        return $this->render('ticket/showTickets.html.twig', [
            'groups' => $groups,
            'adminToken' => $adminToken
        ]);
    }

    #[Route('/group/{linkToken}/ticket/{ticketId}', name: 'group_ticket')]
    public function showTicket(ManagerRegistry $doctrine, Request $request, $ticketId, $linkToken): Response
    {
        $entityManager = $doctrine->getManager();

        $groupId = $request->query->get('groupId');
        $repository = $doctrine->getRepository(Group::class);
        $group = $repository->find($groupId);

        $repository = $doctrine->getRepository(Ticket::class);
        $ticket = $repository->find($ticketId);

        $form = $this->createForm(NewTicketFormType::class, $ticket);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $ticketRequest = $form->get('request')->getData();
            $priorityLevel = $form->get('Priority')->getData();

            $repository = $doctrine->getRepository(Priority::class);
            $priority = $repository->findOneByWeight($priorityLevel);

            $ticket->setPriority($priority);
            $ticket->setRequest($ticketRequest);

            $entityManager->persist($ticket);
            $entityManager->flush();

            return $this->redirectToRoute('group_tickets', ['linkToken' => $linkToken, 'groupId' => $groupId]);
            
        }

        return $this->renderForm('ticket/index.html.twig', [
            'form' => $form,
            'group' => $group,
            'ticketId' => $ticketId,
            'linkToken' => $linkToken
        ]);
    }

    #[Route('/event/{adminToken}/tickets/{ticketId}', name: 'edit_ticket')]
    public function editTicket(ManagerRegistry $doctrine, Request $request, string $adminToken, string $ticketId): Response
    {
        $entityManager = $doctrine->getManager();

        $repository = $doctrine->getRepository(Ticket::class);
        $ticket = $repository->find($ticketId);
        $group = $ticket->getIdGroup();
        $event = $group->getEvent();

        $form = $this->createForm(EditTicketFormType::class, $ticket);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $status = $form->get('Status')->getData();
            $now = new \DateTime(); 

            $ticket->setStatus($status);
            if($status->getIsArchived()) 
            {
                $group->setLastArchivedAt($now);
            } else {
                $group->setLastArchivedAt(null);
            }

            $entityManager->persist($ticket);
            $entityManager->flush();

            return $this->redirectToRoute('event_tickets', ['adminToken' => $adminToken, 'eventId' => $event->getId()]);
            
        }

        return $this->renderForm('ticket/editTicket.html.twig', [
            'form' => $form,
            'ticket' => $ticket,
            'adminToken' => $adminToken
        ]);
    }

    #[Route('/group/{linkToken}/ticket/{ticketId}/remove', name: 'remove_ticket')]
    public function removeTicket(ManagerRegistry $doctrine, Request $request, string $ticketId, string $linkToken): Response
    {
        $groupId = $request->query->get('groupId');

        $repository = $doctrine->getRepository(Ticket::class);
        $ticket = $repository->find($ticketId);
        
        $repository->remove($ticket);

        return $this->redirectToRoute('group_tickets', ['linkToken' => $linkToken, 'groupId' => $groupId, 'ticketId' => $ticketId]);  
    }

    
}
